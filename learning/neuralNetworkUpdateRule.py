import numpy as np

class NeuralNetworkUpdateRule():
    def __init__(self, name, lossFunction, learning_rate):
        self.name = name
        self.loss_function = lossFunction
        self.learning_rate = learning_rate

    def __str__(self):
        return self.name + "(eta=%s)" % (str(self.learning_rate))

    def update(self, model, record, prediction_score, true_label, currentRound):
        model.addExample(record, true_label)
        if model.batchFull():
            records, labels = model.getExamples()
            #h = model.core.fit(np.asarray(records), np.asarray(labels), batch_size=model.batch_size, epochs=1, verbose=False)
            #print("Loss ", h.history['loss'][0])
            model.core.train_on_batch(np.asarray(records), np.asarray(labels))
            model.cleanBatch()

    ## should something be done here?
    def getParamRange(self):
        pass

    def setParams(self, params):
        self.learning_rate = params['learning_rate']
