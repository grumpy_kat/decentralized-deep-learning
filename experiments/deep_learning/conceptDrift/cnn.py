from inputs.graphical_models import BshoutyLongModel
from inputs.stoppingConditions import MaxNumberOfExamplesCondition
from learning.neuralNetworkLossFunction import NeuralNetworkLossFunction
from learning.neuralNetworkUpdateRule import NeuralNetworkUpdateRule
from learning.DNNModel import FullyConnectedModel, arithmetic_mean
from environments import PredictionEnvironment
from synch.synchronization import NoSyncOperator, CentralSyncOperator, StaticSyncOperator, HedgedDistBaseSync
import experiment

def main():
    numberOfNodes   = 100
    dim             = 50
    anchorBatchSize = 10
    
    inputStream     = BshoutyLongModel(dim=dim, drift_prob=0.001,num_nodes=numberOfNodes)
    learningRate    = 0.1
    lossFunction    = NeuralNetworkLossFunction('binary_crossentropy')
    updateRule      = NeuralNetworkUpdateRule('SGD', lossFunction, learningRate)
    updateRule2     = NeuralNetworkUpdateRule('SGD', lossFunction, 0.01)
    
    model     = FullyConnectedModel(update_rule = updateRule, loss_function = lossFunction, batch_size = 10, dim = dim)
    model2     = FullyConnectedModel(update_rule = updateRule2, loss_function = lossFunction, batch_size = 10, dim = dim)
    
    envs    =   [
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule2,
                                         model           = model2,
                                         syncOperator    = NoSyncOperator(),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule2,
                                         model           = model2,
                                         syncOperator    = CentralSyncOperator(),
                                         serial          = True,
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule,
                                         model           = model,
                                         batchSizeInMacroRounds  = anchorBatchSize,
                                         syncOperator    = StaticSyncOperator(),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule,
                                         model           = model,
                                         batchSizeInMacroRounds  = anchorBatchSize,
                                         syncOperator            = HedgedDistBaseSync(0.3),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule,
                                         model           = model,
                                         batchSizeInMacroRounds  = anchorBatchSize*2,
                                         syncOperator    = StaticSyncOperator(),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule,
                                         model           = model,
                                         batchSizeInMacroRounds  = anchorBatchSize,
                                         syncOperator            = HedgedDistBaseSync(0.7),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule,
                                         model           = model,
                                         batchSizeInMacroRounds  = anchorBatchSize*4,
                                         syncOperator    = StaticSyncOperator(),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule,
                                         model           = model,
                                         batchSizeInMacroRounds  = anchorBatchSize,
                                         syncOperator            = HedgedDistBaseSync(1),
                                         modelAggregation= arithmetic_mean),
                 ]
				 
    experiment.run(inputStream, envs, MaxNumberOfExamplesCondition(numberOfNodes*5000))

if __name__ == "__main__":
    main()

