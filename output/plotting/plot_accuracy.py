'''
Created on 20.01.2013

@author: Mario Boley
'''
import os
import matplotlib.pyplot as plt
from output.parseOutput import read_times_file, read_results_column, ENV_ID_COLUMN, ENV_LABEL_COLUMN, RESULT_FILENAME
from output.plotting.plotting import FILE_FORMAT, save_chart, plot_drift_times, generate_color
from utils import signum
from learning.lossFunction import ZeroOneClassificationLoss
import re
import numpy as np


def generate(experiment_root_folder, show_drift_times, show_sync_times, loss_function, per_node=False):
    print("Plotting Accuracy")
    if per_node:
        plot_per_node_chart(experiment_root_folder, show_drift_times, show_sync_times, loss_function)
    else:
        plot_chart(experiment_root_folder, show_drift_times, show_sync_times, loss_function)
    plt.xlabel("Time")
    plt.ylabel("Accuracy")
    save_chart(experiment_root_folder + "charts/accuracy_time." + FILE_FORMAT)

# Chart generation functions:
def plot_chart(experiment_root_folder, show_drift_times, show_sync_times, loss_function):
    envs = read_results_column(experiment_root_folder + RESULT_FILENAME, ENV_ID_COLUMN)
    labels = read_results_column(experiment_root_folder + RESULT_FILENAME, ENV_LABEL_COLUMN)
    index = -1
    max_y = 0
    max_x = 0
    for env in envs:
        index += 1
        metric = read_accuracy(experiment_root_folder + 'logs/prediction_%s.log' % (env), loss_function)
        write_metric(experiment_root_folder, metric, env)
        xpoints, ypoints = metric
        max_env_x = max(xpoints)
        max_env_y = max(ypoints)
        if max_env_x > max_x: max_x = max_env_x
        if max_env_y > max_y: max_y = max_env_y
        color = generate_color(index, len(envs))
        line = plt.plot(xpoints, ypoints, label=labels[index], color=color)
        plt.setp(line, 'linewidth', 2.0)
        if show_sync_times: plot_sync_times(experiment_root_folder, env, color, (xpoints, ypoints))
    plt.xlim(0, max_x)
    plt.ylim(0, max_y * 1.05)
    if show_drift_times: plot_drift_times(experiment_root_folder)

def plot_per_node_chart(experiment_root_folder, show_drift_times, show_sync_times, loss_function):
    envs = read_results_column(experiment_root_folder + RESULT_FILENAME, ENV_ID_COLUMN)
    labels = read_results_column(experiment_root_folder + RESULT_FILENAME, ENV_LABEL_COLUMN)
    max_y = 0
    max_x = 0
    index = -1
    colors = ColorUtil().generateDistinctColors(len(envs))
    for env in envs:
        index += 1
        metric = read_per_node_accuracy(experiment_root_folder + 'logs/prediction_%s.log' % (env), loss_function)
        #write_metric(experiment_root_folder, metric)
        xpoints, ypoints_per_node = metric
        max_env_x = max(xpoints)
        #max_env_y = max(ypoints)
        if max_env_x > max_x: max_x = max_env_x
        #if max_env_y > max_y: max_y = max_env_y
        for node in ypoints_per_node:
            line = plt.plot(xpoints, ypoints_per_node[node], label=labels[index], color=colors[index])
            plt.setp(line, 'linewidth', 2.0)
            if show_sync_times: plot_sync_times(experiment_root_folder, env, colors[index], (xpoints, ypoints_per_node[node]))
    plt.xlim(0, max_x)
    #plt.ylim(0, max_y * 1.05)
    #if show_drift_times: plot_drift_times(experiment_root_folder)

def write_metric(experiment_root_folder, metric, env_name):
    file_name = experiment_root_folder + "/charts/accuracy_" + env_name + ".txt"
    rounds, values = metric
    f = open(file_name, 'w')
    for idx in range(len(values)):
        round_number = rounds[idx]
        value = values[idx]
        f.write("%d\t%.5f\n" % (round_number, value))
    f.close()


def plot_sync_times(experiment_root_folder, env, color, metric):
    result_xpoints = []
    result_ypoints = []
    xpoints, ypoints = metric
    if os.path.isfile(experiment_root_folder + "logs/communication_%s.log" % env):
        sync_times = read_times_file(experiment_root_folder + "logs/communication_%s.log" % env)

        for idx in range(len(ypoints)):
            x = xpoints[idx]
            if x in sync_times:
                y = ypoints[idx]
                result_xpoints.append(x)
                result_ypoints.append(y)

        plt.scatter(result_xpoints, result_ypoints, s=sync_times_size, c=color, marker=sync_times_marker)

def read_accuracy(file_name, loss_function):
    rounds = []
    values = []
    corrects = []
    total = 0
    roundValues = []
    currentRound = 0
    curNode = 0
    handle = open(file_name, 'r')
    for line in handle:
        parts = line.strip().split("\t")
        round_number = int(parts[0])
        if round_number > currentRound:
            rounds.append(round_number)
            currentRound = round_number
            total += 1
            if len(roundValues) == 0:
                values.append(0)
            else:
                values.append(sum(roundValues) / (1.0 * len(roundValues)))
            roundValues = []
            curNode =0
        if len(corrects) <= curNode:
            corrects.append(0)
        if "[" in parts[1] or "[" in parts[2]:
            arr1 = np.array([float(v) for v in cleanString(parts[1]).split()])
            arr2 = np.array([float(v) for v in cleanString(parts[2]).split()])
            if loss_function.name == 'categorical_crossentropy':
                if np.argmax(arr1) == np.argmax(arr2):
                    corrects[curNode] += 1
            if loss_function.name == 'mse':
                corrects[curNode] += 2 - abs(arr1[0] - arr2[0])
            if loss_function.name == 'binary_crossentropy':
                if (arr1[0] == 0.0 and arr2[0] < 0.5) or (arr1[0] == 1.0 and arr2[0] >= 0.5):
                    corrects[curNode] += 1
        else:
            if signum(float(parts[1])) == signum(float(parts[2])):
                corrects[curNode] += 1
        roundValues.append(float(corrects[curNode]) / total)
        curNode += 1

    handle.close()
    return (rounds, values)

def read_per_node_accuracy(file_name, loss_function):
    rounds = []
    nodes = []
    values = {}
    correct = []
    total = 0
    cur_node = 0
    currentRound = 0
    handle = open(file_name, 'r')
    for line in handle:
        parts = line.strip().split("\t")
        round_number = int(parts[0])
        if round_number > currentRound:
            rounds.append(round_number)
            currentRound = round_number
            cur_node = 0
            total += 1
        if round_number == currentRound:
            if len(correct) <= cur_node:
                correct.append(0)
            if "[" in parts[1] or "[" in parts[2]:
                arr1 = np.array([float(v) for v in cleanString(parts[1]).split()])
                arr2 = np.array([float(v) for v in cleanString(parts[2]).split()])
                if loss_function.name == 'categorical_crossentropy':
                    if np.argmax(arr1) == np.argmax(arr2):
                        correct[cur_node] += 1
                if loss_function.name == 'mse':
                    correct[curNode] += 1 - abs(arr1[0] - arr2[0])
                if loss_function.name == 'binary_crossentropy':
                    if (arr1[0] == 0.0 and arr2[0] < 0.5) or (arr1[0] == 1.0 and arr2[0] >= 0.5):
                        correct[cur_node] += 1
            else:
                if signum(float(parts[1])) == signum(float(parts[2])):
                    correct[cur_node] += 1
            total += 1
            if values.get(cur_node) == None:
                values[cur_node] = []
            values[cur_node].append(float(correct[cur_node]) / total)
            cur_node += 1
    handle.close()
    return (rounds, values)

def cleanString(str):
    ret = re.sub("\s+", " ", str.replace("[","").replace("]","").strip())
    return ret

show_drift_times = True
show_sync_times = True
sync_times_marker = 'x'
sync_times_size = 60

if __name__ == "__main__":
    generate("./testdata/", show_drift_times, show_sync_times)