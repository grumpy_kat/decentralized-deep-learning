from inputs import InputStream
from utils import sparse_vector
import numpy as np
import config
import os

class CnnMnist(InputStream):
    def __init__(self, data_file, nodes = 1, loop_file=True):
        InputStream.__init__(self, "MNIST_images", nodes)
        self.file_name = os.path.join(config.PATH_TO_MNIST_IMAGES, data_file)
        self.input_file_handle = open(self.file_name, "r")
        self.current_line = self._fetch_line()
    
    def has_more_examples(self):
        if len(self.current_line.split(',')) == 28*28 + 1:
			if not loop_file:
				return False
			self.input_file_handle = open(self.file_name, "r")
			self.current_line = self._fetch_line()
		return True
    
    def _fetch_line(self):
        return self.input_file_handle.next()
        
    def _generate_example(self):
        sequence, target = self._decode_line(self.current_line)
        self.current_line = self._fetch_line()
        
        return (sequence, target)
    
    def _decode_line(self, line):
        parsed_line = [int(c) for c in line.split(',')]
        image = np.asarray(parsed_line[1:], dtype='float32').reshape(28,28,1) / 255
        label = np.zeros(10)
        label[int(parsed_line[0])] = 1
            
        return image, label
    
    def close(self):
        self.input_file_handle.close()
        
if __name__ == "__main__":
    stream = CnnMnist(data_file = 'mnist_train.csv')
    count = 0
    print("Reading stream")
    while stream.has_more_examples():
        ex, label = stream.generate_example()
        if count % 1000 == 0:
            print( ex)
            print( label)
        count += 1

