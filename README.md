# Decentralized Deep Learning

This is a quick guide on how to use the experiment framework for decentralized deep learning and the dynamic averaging protocol. 

## Content ##

* download datasets
* get the code running
* run an experiment

## Download Datasets ##

Experiments are conducted using the MNIST classification dataset. You can download the dataset in CSV format from [here](https://pjreddie.com/projects/mnist-in-csv/). 
Note that we have used both training and test set together for our experiments; data is presented to the learners in a streaming fashion. The csv file has to be put into the folder "/data/cnn_mnist/".
For testing, whether the data can be loaded correctly, you can run the file "/inputs/cnnMnist.py".
For the concept drift experiments, a synthetic dataset is created on the fly. 

## Get the Code Running ##

Besides standard python libraries, the framework requires tensorflow and keras. In principle, the code is compatible with both Python 2.7 and 3.x. 

## Run an Experiment ##

Experiments are described as python scripts. You can find the descriptions for the MNIST and Concept Drift experiments in the folder "/experiments/". An experiment is conducted by running the corresponding python script (e.g., "/experiments/deep_learning/MNIST/cnn.py").
After running, a subfolder will be created (named by the timestamp of experiment start) that contains log files and results, as well as a plotting script.